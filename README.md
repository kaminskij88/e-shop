# E-Shop

## Description

Simple "shop" application, with possibility to fetch products from database and add them to cart.

## Used technologies

- React
- Context Api
- Hooks
- Axios
- Material UI
- Atomic design
- TypeScript
- Storybook

Application is supported by Firebase API and Database.
