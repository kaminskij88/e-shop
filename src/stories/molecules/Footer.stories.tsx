import { Story, Meta } from "@storybook/react";
import Footer from "../../components/molecules/Footer";

export default {
  title: "Molecule/Footer",
  component: Footer,
} as Meta;

const Template: Story = (args) => <Footer {...args} />;

export const Default = Template.bind({});
Default.args = {
  color: "red",
};
