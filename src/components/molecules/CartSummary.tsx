import { FC } from "react";

export interface CartSummaryProps {
  totalPrice: number;
}
const CartSummary: FC<CartSummaryProps> = ({ totalPrice }) => {
  return <div> {totalPrice} </div>;
};

export default CartSummary;
