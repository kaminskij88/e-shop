import { FC } from "react";
import {
  AppBar,
  Toolbar,
  makeStyles,
  Typography,
  Button,
  Badge,
} from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useContext } from "react";
import { CartContext } from "../../context/cart_context";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  appBar: {
    backgroundColor: "#14213d",
  },
  toolbar: {
    display: "grid",
    gridTemplateColumns: "auto 6rem",
  },
});

const Navbar: FC = () => {
  const { productNumber } = useContext(CartContext);

  const classes = useStyles();
  return (
    <>
      <AppBar className={classes.appBar} position="static">
        <Toolbar className={classes.toolbar}>
          <Link to="/">
            <Typography variant="h6">E-Shop</Typography>
          </Link>
          <Badge color="secondary" badgeContent={productNumber}>
            <Link to="/cart">
              <Button
                variant="contained"
                color="primary"
                startIcon={<ShoppingCartIcon />}
              >
                Cart
              </Button>
            </Link>
          </Badge>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navbar;
