import React from "react";
import { FC } from "react";
import { useContext } from "react";
import { ProductsContext } from "../../context/context";
import { makeStyles } from "@material-ui/styles";
import palette from "../../theme/palette";
import { Button } from "@material-ui/core";
import { CartContext } from "../../context/cart_context";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "grid",
    margin: "1rem",
    boxShadow: "0px 0px 23px -9px rgba(0,0,0,0.6)",
    gridTemplateRows: "4rem auto 4rem",
    "& p": {
      color: palette.text.primary,
      textAlign: "center",
    },
  },
  header: {
    backgroundColor: "#14213d",
    "& h2": {
      color: palette.text.secondary,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  },
  details: {
    display: "grid",
    gridTemplateColumns: "7rem auto",
    gridTemplateRows: "4rem auto",
  },
  labels: {
    margin: "1rem",
    fontWeight: "bold",
  },
  detailsText: {
    margin: "1rem",
  },
  footer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#14213d",
  },
}));

const ProductDetails: FC = () => {
  const { addProduct } = useContext(CartContext);

  const classes = useStyles();

  const { product } = useContext(ProductsContext);

  return (
    <>
      <div className={classes.container}>
        {product ? (
          <>
            <div className={classes.header}>
              <h2>{product.name}</h2>
            </div>
            <div className={classes.details}>
              <div>
                <div className={classes.labels}>Price:</div>
              </div>

              <div>
                <div className={classes.detailsText}>{product.price}</div>
              </div>
              <div>
                <div className={classes.labels}>Description:</div>
              </div>
              <div>
                <div className={classes.detailsText}>{product.description}</div>
              </div>
            </div>
          </>
        ) : (
          <p>Choose Product</p>
        )}

        <div className={classes.footer}>
          {product && (
            <Button
              color="primary"
              size="large"
              variant="contained"
              onClick={() => addProduct(product)}
            >
              Add to cart
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default React.memo(ProductDetails);
