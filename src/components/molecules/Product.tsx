import { FC, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Typography } from "@material-ui/core";
import { useContext } from "react";
import { ProductsContext } from "../../context/context";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import React from "react";

const useStyles = makeStyles({
  "@keyframes active": {
    "0%": {
      backgroundColor: "#E5EAFA",
    },
    "100%": {
      backgroundColor: "#403f4c",
    },
  },
  "@keyframes deactivate": {
    "0%": {
      backgroundColor: "#403f4c",
    },
    "100%": {
      backgroundColor: "#E5EAFA",
    },
  },
  productElement: {
    display: "grid",
    backgroundColor: "#E5EAFA",
    opacity: "1rem",
    borderColor: "black",
    width: "100%",
    borderRadius: "1rem",
    boxShadow: "0px 0px 23px -9px rgba(0,0,0,0.6)",
    animation: "$deactivate 200ms ease-out forwards",
    "& h3": { color: "black" },
  },
  cardContent: {
    display: "grid",
    alignContent: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  active: {
    animation: "$active 200ms ease-in-out forwards",
    "& h3": { color: "white" },
  },
});

export interface productBox {
  key: string;
  value: { name: string; description: string; price: number };
}

const Product: FC<productBox> = ({ value }) => {
  const [active, setActive] = useState(false);
  const { selectProduct } = useContext(ProductsContext);

  const classes = useStyles();

  return (
    <>
      <Card className={`${classes.productElement} ${active && classes.active}`}>
        <CardActionArea
          onMouseEnter={() => setActive(true)}
          onMouseLeave={() => setActive(false)}
          onClick={() => {
            // setActive(true);
            selectProduct(value);
          }}
        >
          <CardContent className={classes.cardContent}>
            <Typography variant="h3">{value.name}</Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </>
  );
};

export default React.memo(Product);
