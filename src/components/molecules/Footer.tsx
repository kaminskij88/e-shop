import { FC } from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  appFooter: {
    backgroundColor: "#14213d",
    height: "2.5rem",
    zIndex: 1,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    boxShadow:
      "0px -2px 4px 0px rgba(0,0,0,0.2),0px -4px 5px 0px rgba(0,0,0,0.14),0px -1px 10px 0px rgba(0,0,0,0.12)",
  },
});

const Footer: FC = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.appFooter}>
        <Typography variant="body2">© Copyright - JK</Typography>
      </div>
    </>
  );
};

export default Footer;
