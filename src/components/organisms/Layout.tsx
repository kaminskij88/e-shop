import { FC } from "react";
import Navbar from "../molecules/Navbar";
import Footer from "../molecules/Footer";

const Layout: FC = (props) => {
  return (
    <>
      <Navbar />
      {props.children}
      <Footer />
    </>
  );
};

export default Layout;
