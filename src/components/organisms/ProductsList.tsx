import { FC, useContext } from "react";
import Product from "../molecules/Product";
import { makeStyles } from "@material-ui/styles";
import { ProductsContext } from "../../context/context";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles({
  productsGrid: {
    margin: "1rem",
    display: "grid",
    gridTemplateColumns: "30% 30% 30%",
    gridTemplateRows: "7rem 7rem 7rem",
    gridGap: "1rem",
  },
  spinner: {
    display: "grid",
    padding: "5rem",
    justifyContent: "center",
  },
});

const ProductsList: FC = () => {
  const { products, isFetching } = useContext(ProductsContext);
  const classes = useStyles();

  const productsList = products.map(
    (product: {
      key: string;
      value: { name: string; description: string; price: number };
    }) => {
      return <Product key={product.key} value={product.value} />;
    }
  );

  return (
    <>
      {isFetching ? (
        <div className={classes.spinner}>
          <CircularProgress />
        </div>
      ) : (
        <div className={classes.productsGrid}>{productsList}</div>
      )}
    </>
  );
};

export default ProductsList;
