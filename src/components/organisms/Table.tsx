import { makeStyles } from "@material-ui/core/styles";
import { Table as MaterialUiTable } from "@material-ui/core";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { FC } from "react";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export interface CartTableProps {
  cart: {
    id: number;
    name: string;
    description: string;
    price: number;
    sku: number;
  }[];
}

const Table: FC<CartTableProps> = ({ cart }) => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <MaterialUiTable className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>Description</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {cart.map((product) => (
            <TableRow key={product.sku}>
              <TableCell component="th" scope="row">
                {product.name}
              </TableCell>
              <TableCell align="right">{product.price}</TableCell>
              <TableCell>{product.description}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </MaterialUiTable>
    </TableContainer>
  );
};

export default Table;
