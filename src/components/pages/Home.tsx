import { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ProductsList from "../organisms/ProductsList";
import ProductDetails from "../molecules/ProductDetails";

const useStyles = makeStyles({
  homeContainer: {
    display: "grid",
    gridTemplateColumns: "60% 40%",
  },
});

const Home: FC = () => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.homeContainer}>
        <div>
          <ProductsList />
        </div>
        <div>
          <ProductDetails />
        </div>
      </div>
    </>
  );
};

export default Home;
