import { FC } from "react";
import { useContext } from "react";
import { CartContext } from "../../context/cart_context";
import Table from "../organisms/Table";
import CartSummary from "../molecules/CartSummary";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  cartContainer: {
    display: "grid",
    gridTemplateColumns: "70% 30%",
  },
});

const Cart: FC = () => {
  const classes = useStyles();

  const { cart, totalPrice } = useContext(CartContext);

  return (
    <div className={classes.cartContainer}>
      <Table cart={cart} />
      <CartSummary totalPrice={totalPrice} />
    </div>
  );
};

export default Cart;
