import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core";
import theme from "../src/theme/index";
import Layout from "./components/organisms/Layout";
import "./index.css";
import { ProductsProvider } from "./context/context";
import { CartProvider } from "./context/cart_context";

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <ProductsProvider>
        <CartProvider>
          <Layout>
            <App />
          </Layout>
        </CartProvider>
      </ProductsProvider>
    </BrowserRouter>
  </ThemeProvider>,
  document.getElementById("root")
);
