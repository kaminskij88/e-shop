import { ADD_PRODUCT } from "../reducers/actions";

const cartReducer = (state, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        cart: [...state.cart, action.payload],
        productNumber: state.productNumber + 1,
        totalPrice: state.totalPrice + action.payload.price,
      };
    default:
      console.log("default");
  }
};

export default cartReducer;
