import {
  SELECT_PRODUCT,
  GET_PRODUCTS,
  START_FETCHING,
} from "../reducers/actions";

const productsReducer = (state, action) => {
  switch (action.type) {
    case SELECT_PRODUCT:
      return { ...state, product: action.payload };
    case START_FETCHING:
      return { ...state, isFetching: true };
    case GET_PRODUCTS:
      return { ...state, products: action.payload, isFetching: false };
    default:
      console.log("default");
  }
};

export default productsReducer;
