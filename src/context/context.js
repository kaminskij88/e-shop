import React, { useReducer, createContext, useEffect } from "react";
import productsReducer from "../reducers/productsReducer";
import {
  SELECT_PRODUCT,
  GET_PRODUCTS,
  START_FETCHING,
} from "../reducers/actions";
import { getProducts as getProductsApi } from "../api/productApi";

const initialState = {
  products: [],
  product: false,
  isFetching: false,
};

const ProductsContext = createContext();

const ProductsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(productsReducer, initialState);

  const getProducts = async () => {
    dispatch({ type: START_FETCHING });
    try {
      const products = await getProductsApi();
      const fetchedProducts = [];
      Object.entries(products).forEach((product) => {
        fetchedProducts.push({ key: product[0], value: product[1] });
      });
      dispatch({ type: GET_PRODUCTS, payload: fetchedProducts });
    } catch (error) {}
  };

  const selectProduct = (product) => {
    dispatch({ type: SELECT_PRODUCT, payload: product });
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <ProductsContext.Provider value={{ ...state, selectProduct }}>
      {children}
    </ProductsContext.Provider>
  );
};

export { ProductsContext, ProductsProvider };
