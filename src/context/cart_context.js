import React, { useReducer, createContext } from "react";
import cartReducer from "../reducers/cartReducer";
import { ADD_PRODUCT } from "../reducers/actions";

const initialState = {
  cart: [],
  productNumber: 0,
  totalPrice: 0,
};

const CartContext = createContext();

const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initialState);

  const addProduct = (product) => {
    dispatch({ type: ADD_PRODUCT, payload: product });
  };
  return (
    <CartContext.Provider value={{ ...state, addProduct }}>
      {children}
    </CartContext.Provider>
  );
};

export { CartContext, CartProvider };
