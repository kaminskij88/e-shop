import { FC, lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Home from "./components/pages/Home";

const Cart = lazy(() => import("./components/pages/Cart"));

const useStyles = makeStyles({
  spinnerContainer: {
    height: "100",
  },
  spinner: {
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50% -50%)",
  },
});

const App: FC = () => {
  const classes = useStyles();
  return (
    <Suspense
      fallback={
        <div className={classes.spinnerContainer}>
          <CircularProgress className={classes.spinner} />
        </div>
      }
    >
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/cart" exact>
          <Cart />
        </Route>
      </Switch>
    </Suspense>
  );
};

export default App;
