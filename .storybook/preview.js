import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core";
import theme from "../src/theme/index";
import { ProductsProvider } from "../src/context/context";
import { CartProvider } from "../src/context/cart_context";
import "../src/index.css";

export const decorators = [
  (Story) => {
    return (
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <ProductsProvider>
            <CartProvider>
              <Story />
            </CartProvider>
          </ProductsProvider>
        </BrowserRouter>
      </ThemeProvider>
    );
  },
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: { expanded: true },
};
